
# ------------------------------------ Locale settings
export LC_ALL=en_US.UTF-8

# ------------------------------------ Boxname
function box_name {
    [ -f ~/.box-name ] && cat ~/.box-name || echo $SHORT_HOST || echo $HOST
}

# ------------------------------------ Vagrant state.
vagrant_state () {
    if [ -f ./Vagrantfile ] && [ -f .vagrant/machines/default/virtualbox/id ]; then
        runningVMS=`vagrant status | grep '(virtualbox)' | awk '{print $2}'`
        if [ "$runningVMS" = "running" ]; then
            echo "<= ON"
        else
            echo "<= $runningVMS "
        fi
    elif [ -f ./Vagrantfile ]; then
        echo "=> it's dead Jim "
    fi
}

# ------------------------------------ Vagrant IPs
vagrant_info () {
    vagrant_state=$(vagrant_state)
    echo ${vagrant_state}
}

# ------------------------------------ AWS Profile assumed
aws_profile () {
    if ! [ "$AWS_PROFILE" = "" ]; then
        echo -e "\\033[38;5;214m"${AWS_PROFILE}"\\033[0m "
    fi
}

# ------------------------------------ LS with git branch.
lg () {
    ls -alF "$@" | gawk '{match($0,/^(\S+\s+){8}(.+)\/$/,f);b="";c="git -C \""f[2]"\" branch 2>/dev/null";while((c|getline g)>0){if(match(g,/^\* (.+)$/,a)){b="("a[1]")"}};close(c);print$0,b}';
}

# ------------------------------------ Git clone with owner in folder structure.
git_owner_clone () {
    if [[ $1 =~ ^(git\@|https?\:\/\/)(.*[:|\/])(.*)\/(.*)$ ]]; then
        PROTO=${match[1]}
        PROVIDER=${match[2]}
        OWNER=${match[3]}
        REPO=${match[4]}
        DIRECTORY=${PWD##*/}

        if [ -h "../${DIRECTORY}" ]; then
            DIRECTORY=$(readlink ../${PWD##*/})
        fi

        if [ `echo "${DIRECTORY}" | awk '{print tolower($0)}'` != `echo "${OWNER}" | awk '{print tolower($0)}'` ]; then
            echo "${DIRECTORY}"
            mkdir -p "${OWNER}"
            cd ${OWNER}
        fi

        git clone $1

        cd ${REPO%\.git}
    else
        echo 'NO MATCH'
        echo "${BASH_REMATCH}"
    fi
}

# ------------------------------------ Get SSH authorized_keys Fingerprints
function fingerprints() {
    local file="${1:-$HOME/.ssh/authorized_keys}"
    while read l; do
        [[ -n $l && ${l###} = $l ]] && ssh-keygen -l -f /dev/stdin <<<$l
    done < "${file}"
}


# ------------------------------------ History
wiki() {
    if [ "$1" != '' ]; then
        history | grep -i "$1"
    else
        history
    fi
}

# ------------------------------------ Mealie-crypt values get
mcg() {
    if [ "$1" != '' ]; then
        mealie-crypt values get | grep -i "$1"
    else
        mealie-crypt values get
    fi
}

# ------------------------------------ Convert JSON to CSV
json2csv() {
    cat $1 | jq -r '(map(keys) | add | unique) as $cols | map(. as $row | $cols | map($row[.])) as $rows | $cols, $rows[] | @csv'
}

# ------------------------------------ Create a new case folder for working PS cases
export CASEHOME="$HOME/Documents/Workitems/Cases"
cases() {
    CASENUMBER=$1
    if [ "${CASENUMBER}" != '' ]; then
        if [ `ls $CASEHOME | grep $CASENUMBER` ]; then
            CASEDIR=$CASEHOME/`ls $CASEHOME | grep $CASENUMBER`
        else
            mkcase $CASENUMBER
        fi
        cd $CASEDIR
    else
        echo 'Usage `cases case_number`'
        cd $CASEHOME
    fi
}

mkcase() {
    CASENUMBER=$1
    MAKEFOLDERS=$2
    if [ "${CASENUMBER}" != '' ]; then
        DATE=`date +"%Y%m%d"`
        if [ `ls $CASEHOME | grep $CASENUMBER` ]; then
            CASEDIR=$CASEHOME/`ls $CASEHOME | grep $CASENUMBER`
            echo $CASEDIR found
            VALID='nN '
            if [[ ! "${MAKEFOLDERS}" =~ [^$VALID] ]]; then
                mkdir -p $CASEDIR/{Notes,Files,tmp}
                touch $CASEDIR/Notes/case_$CASENUMBER.txt
            fi
        else
            CASEDIR=$CASEHOME/$CASENUMBER-$DATE
            echo $CASEDIR created
            mkdir -p $CASEDIR/{Notes,Files,tmp}
            touch $CASEDIR/Notes/case_$CASENUMBER.txt
            echo "$DATE: Case ${CASENUMBER}: __________" >> $CASEDIR/Notes/case_$CASENUMBER.txt
        fi
        cases $CASENUMBER
        code $CASEDIR
        open $CASEDIR
    else
        echo 'Usage: `mkcase case_number` [make_subfolders]'
    fi
}

# ------------------------------------ rbenv Enviromental include (Ruby)
rbenvactivate () {
    export GEM_HOME="$HOME/.gem"
    export PATH="$HOME/.rbenv/shims:$PATH"
    export RUBY_CONFIGURE_OPTS="--with-openssl-dir=$(brew --prefix openssl@1.1)"
    if ! type rbenv > /dev/null; then
        eval "$(rbenv init -)"
    fi
}

# ------------------------------------ NVM Environmental include (Node)
nvmactivate () {
    NVM_INS=$(brew --prefix nvm)
    [ -s "${NVM_INS}/nvm.sh" ] && echo 'Loading NVM'|| echo "NVM not installed. Use 'brew install nvm'" # This when nvm not installed
    export NVM_DIR="$HOME/.nvm"
    [ -s "${NVM_INS}/nvm.sh" ] && . "${NVM_INS}/nvm.sh"  # This loads nvm
    [ -s "${NVM_INS}/etc/bash_completion.d/nvm" ] && . "${NVM_INS}/etc/bash_completion.d/nvm"  # This loads nvm bash_completion
}

# ------------------------------------ VirtualEnv Environmental include (Python)
# pip should only run if there is a virtualenv currently activated
export PIP_REQUIRE_VIRTUALENV=true
alias pip="pip3"

# Temporarily disable pip virtualenv requirement and install pip module.
gpip () {
    PIP_REQUIRE_VIRTUALENV="" pip "$@"
}

pyenvactivate () {
    # Use pyenv global
    if command -v pyenv 1>/dev/null 2>&1; then
        export PYENV_ROOT="$HOME/.pyenv"
        export PATH="$PYENV_ROOT/bin:$PATH"
        export PATH="$PYENV_ROOT/shims:$PATH"
        export PIP_REQUIRE_VIRTUALENV=

        eval "$(pyenv init -)"
        eval "$(pyenv virtualenv-init -)"
    fi
}

#pyenvactivate
#export PATH=$PATH:/Users/catilre/Library/Python/3.6/bin

# ------------------------------------ GoEnv Environmental include (GoLang)
goenvactivate () {
    eval "$(goenv init -)"
}
# GoLang Path Setup (GoLang)
if type go > /dev/null 2>&1; then
    export GOPATH=$(go env GOPATH)
    export PATH=$PATH:$(go env GOPATH)/bin
fi

# ------------------------------------ Azure-CLI bash completion
#if [ ! -f /usr/local/Cellar/azure-cli/current/etc/bash_completion.d/az ]; then
#    cd /usr/local/Cellar/azure-cli && unlink current || true && ln -s `ls` current && cd ~
#fi
#

#if [ -f /usr/local/Cellar/azure-cli/current/etc/bash_completion.d/az ]; then
#   source /usr/local/Cellar/azure-cli/current/etc/bash_completion.d/az
#fi

# ------------------------------------ Mealie Crypt bash completion
#eval "$(mealie-crypt --completion-script-zsh)"

# ------------------------------------ Directory info.
local current_dir='${PWD/#$HOME/~}'

# ------------------------------------ Git info.
local git_info='$(git_prompt_info)$(git_prompt_status)'
ZSH_THEME_GIT_PROMPT_ADDED="%{$fg[green]%}+"
ZSH_THEME_GIT_PROMPT_MODIFIED="%{$fg[magenta]%}!"
ZSH_THEME_GIT_PROMPT_DELETED="%{$fg[red]%}-"
ZSH_THEME_GIT_PROMPT_RENAMED="%{$fg[blue]%}>"
ZSH_THEME_GIT_PROMPT_UNMERGED="%{$fg[cyan]%}#"
ZSH_THEME_GIT_PROMPT_UNTRACKED="%{$fg[yellow]%}?"

ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg[cyan]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX=""
ZSH_THEME_GIT_PROMPT_DIRTY=""
ZSH_THEME_GIT_PROMPT_CLEAN=" %{$fg[green]%}✔"

# ------------------------------------ Vagrant info
# Get the current state of the vagrant vm in this folder
local vagrant_info='$(vagrant_info)'

# ------------------------------------ Prompts
# Left Prompt

local ret_status="%(?:%{$fg_bold[green]%}➜ :%{$fg_bold[red]%}➜ )"
PROMPT="%{$terminfo[bold]$fg[blue]%}# \
%{$fg[cyan]%}%n\
%{$fg[white]%}@\
%{$fg[green]%}$(box_name)\
%{$fg[white]%}:\
%{$terminfo[bold]$fg[yellow]%}${current_dir}%{$reset_color%} \
%{$fg[blue]%}${vagrant_info}%{$reset_color%}\
${git_info}%{$reset_color%}
$(aws_profile)%{$reset_color%}\
%{$terminfo[bold]$fg[red]%}$ ${ret_status} %{$reset_color%}"

# Right Prompt
RPROMPT='[%*]'

# ------------------------------------ Caret colour
if [[ $USER == "root" ]]; then
    CARETCOLOR="red"
else
    CARETCOLOR="white"
fi

# ------------------------------------ Aliases
if type nvim > /dev/null 2>&1; then
  alias vim='nvim'
fi

alias l='ls -lhF'
alias ll='ls -lahF'
alias cp='cp -iv'
alias mv='mv -iv'
alias qfind="find . -name "
alias rsync="rsync -avzP"
alias gpr='hub pull-request'
alias gcl='git_owner_clone'
alias sgit='sudo SSH_AUTH_SOCK="$SSH_AUTH_SOCK" git'
alias ddd="dcfldd statusinterval=10 status=on bs=1024"
alias rpg="postgres -D /usr/local/var/postgres > /dev/null 2>&1"
alias bex="bundle exec"
alias subl="/Applications/Sublime\ Text.app/Contents/SharedSupport/bin/subl"
alias tpl='tail -f -n50 /var/log/php_error_log'

# Docker
alias dlogs='docker logs --tail 100 -f'
alias dslogs='docker service logs --tail 100 -f'
alias dsup='docker service update --force'

# SilverSearcher
alias ag='ag --hidden '
alias ack='ag'
alias grep='ack'

# Mealie-Crypt
alias mc='mealie-crypt'
alias mce='mc encrypt' # -u alex.pullen -k ~/.ssh/alexpull_mbp'
alias mcd='mc decrypt' # -u alex.pullen -k ~/.ssh/alexpull_mbp'
alias mcvg='mcg'

# Powershell
alias pwsh='/usr/local/microsoft/powershell/6/pwsh'
alias powershell='pwsh'

# Speed up time maching backup by not throttling for lower cpu usage.
alias tmturbo='sudo sysctl debug.lowpri_throttle_enabled=0'
alias tmsnail='sudo sysctl debug.lowpri_throttle_enabled=1'
alias timemachineturbo='tmturbo'
alias timemachinesnail='tmsnail'

# AWS Things
alias _mwinit=/usr/local/bin/mwinit
alias mwinit='_mwinit --ssh-public-key ~/.ssh/alexpull_admiral.pub'
alias authaws='kinit && _mwinit --ssh-public-key ~/.ssh/alexpull_admiral.pub'
alias __aws='/usr/local/bin/aws'
alias _aws='__aws --profile alexpull'
alias aws='AWS_PAGER="" _aws --profile alexpull'
alias case='cd $CASEHOME'
authecr='aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 498664601760.dkr.ecr.us-east-1.amazonaws.com'
alias authecr='echo "This will run and auth ECR: \"$authecr\"" && aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 498664601760.dkr.ecr.us-east-1.amazonaws.com'
alias gotoheap='cd ~/workspace/HEAP-AL2/src/Aws-ps-heap-main'
alias heap-prod='ssh aws-ps-heap-prod-1a-6001.iad6.corp.amazon.com'
alias dcv-conn-apcd='dcv-cdd connect alexpull-clouddesk.aka.corp.amazon.com'
alias dcv-sess-apcd='dcv-cdd create-session alexpull-clouddesk.aka.corp.amazon.com'
alias dcv-close-apcd='dcv-cdd close-session alexpull-clouddesk.aka.corp.amazon.com'
# tmp store find store

# Path stuff
#export PATH="$PATH:~/.composer/vendor/bin:~/.composer/vendor/homestead:~/pear/bin/"
export JAVA_HOME="/Library/Java/JavaVirtualMachines/amazon-corretto-8.jdk/Contents/Home"
export PATH=$PATH:$HOME/.toolbox/bin # AWS BuilderToolbox path


