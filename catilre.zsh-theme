# ------------------------------------ Locale settings
export LC_ALL=en_US.UTF-8

# ------------------------------------ Homebrew
export HOMEBREW_PREFIX="/opt/homebrew";
export HOMEBREW_CELLAR="/opt/homebrew/Cellar";
export HOMEBREW_REPOSITORY="/opt/homebrew";
export PATH="/opt/homebrew/bin:/opt/homebrew/sbin${PATH+:$PATH}";
export MANPATH="/opt/homebrew/share/man${MANPATH+:$MANPATH}:";
export INFOPATH="/opt/homebrew/share/info:${INFOPATH:-}";

# ------------------------------------ Boxname
function box_name {
    [ -f ~/.box-name ] && cat ~/.box-name || echo $SHORT_HOST || echo $HOST
}

# ------------------------------------ Vagrant state.
vagrant_state () {
    if [ -d ./.vagrant ]; then
        if ! vagrant status >/dev/null 2>&1; then
            runningVMS="Unsupported"
        elif [ -f .vagrant/machines/default/vmware_desktop/id ]; then
            runningVMS=`vagrant status | grep '(vmware_desktop)' | awk '{print $2}'`
        elif [ -f .vagrant/machines/default/virtualbox/id ]; then
            runningVMS=`vagrant status | grep '(virtualbox)' | awk '{print $2}'`
        fi
        if [ "$runningVMS" = "running" ]; then
            echo "<= ON"
        else
            echo "<= $runningVMS "
        fi
    elif [ -f ./Vagrantfile ]; then
        echo "=> it's dead Jim "
    fi
}

# ------------------------------------ Vagrant IPs
vagrant_info () {
    vagrant_state=$(vagrant_state)
#    if [[ "${vagrant_state}" = '<= ON' ]]; then
#        BOX_NAME=`grep 'config\.vm\.hostname' -i VagrantFile | cut -f2 -d'=' | sed 's/[\"\ ]//g'`
#
#        BOX_IP=`ssh $BOX_NAME 'ip a' | grep eth2 | grep inet | awk '{ print $2 }' | cut -f1 -d'/'`
#        if [[ -n $BOX_IP ]]; then
#            if [[ $1 == 'ip' ]]; then
#                `ssh $BOX_NAME 'sudo ifdown eth2 && sudo ifup eth2'`
#                BOX_IP=`ssh $BOX_NAME 'ip a' | grep eth2 | grep inet | awk '{ print $2 }' | cut -f1 -d'/'`
#            fi
#
#            echo "<= $BOX_IP"
#        else
#            if [[ $1 == 'ip' ]]; then
#                echo '$BOX_NAME has no public IP: '
#            fi
#            echo ${vagrant_state}
#        fi
#    else
#        echo ${vagrant_state}
#    fi
     echo ${vagrant_state}
}

# ------------------------------------ LS with git branch.
lg () {
    ls -alF "$@" | gawk '{match($0,/^(\S+\s+){8}(.+)\/$/,f);b="";c="git -C \""f[2]"\" branch 2>/dev/null";while((c|getline g)>0){if(match(g,/^\* (.+)$/,a)){b="("a[1]")"}};close(c);print$0,b}';
}

# ------------------------------------ Git clone with owner in folder structure.
git_owner_clone () {
    if [[ $1 =~ ^(git\@|https?\:\/\/)(.*[:|\/])(.*)\/(.*)$ ]]; then
        PROTO=${match[1]}
        PROVIDER=${match[2]}
        OWNER=${match[3]}
        REPO=${match[4]}
        DIRECTORY=${PWD##*/}

        if [ -h "../${DIRECTORY}" ]; then
            DIRECTORY=$(readlink ../${PWD##*/})
        fi

        if [ `echo "${DIRECTORY}" | awk '{print tolower($0)}'` != `echo "${OWNER}" | awk '{print tolower($0)}'` ]; then
            echo "${DIRECTORY}"
            mkdir -p "${OWNER}"
            cd ${OWNER}
        fi

        git clone $1

        cd ${REPO%\.git}
    else
        echo 'NO MATCH'
        echo "${BASH_REMATCH}"
    fi
}

# ------------------------------------ Get SSH authorized_keys Fingerprints
function fingerprints() {
    local file="${1:-$HOME/.ssh/authorized_keys}"
    while read l; do
        [[ -n $l && ${l###} = $l ]] && ssh-keygen -l -f /dev/stdin <<<$l
    done < "${file}"
}

# ------------------------------------ Ansible vault password
export ANSIBLE_VAULT_PASSWORD_FILE=/Volumes/britehouse-info/a_v_f
avds() {
    echo $1 > /tmp/1
    tail -n +2 /tmp/1 > /tmp/2
    sed 's/  //g' /tmp/2 > /tmp/1

    ansible-vault decrypt < /tmp/1

    rm -f /tmp/{1,2}
}

# ------------------------------------ Salt gnupg encrypt
sges() {
    echo -n $1 | docker run --rm -i -a stdout britehousemobilecontainers.azurecr.io/britehousemobile/devops_salt-master-gpg
}
sgds() {
    echo "$1" | gpg -d
}

sgds2() {
    echo $1 > /tmp/1
    tail -n +2 /tmp/1 > /tmp/2
    sed 's/  //g' /tmp/2 > /tmp/1

    gpg -d < /tmp/1

    rm -f /tmp/{1,2}
}

# ------------------------------------ Ignore header linux linux foo
# cat mystuff.txt |body grep 'warren' |body sort
body () {
    IFS= read -r header
    printf '%s\n' "$header"
    "$@"
}

# ------------------------------------ History
wiki() {
    if [ "$1" != '' ]; then
        history | grep -i "$1"
    else
        history
    fi
}

# ------------------------------------ Mealie-crypt values get
mcg() {
    if [ "$1" != '' ]; then
        mealie-crypt values get | grep -i "$1"
    else
        mealie-crypt values get
    fi
}

json2csv() {
    cat $1 | jq -r '(map(keys) | add | unique) as $cols | map(. as $row | $cols | map($row[.])) as $rows | $cols, $rows[] | @csv'
}

# ------------------------------------ Shell (ZSH) settings -> These actually belong in .zshrc
#export EDITOR=`which nvim` # Make vim default editor
#COMPLETION_WAITING_DOTS="true" # Display red dots whilst waiting for completion.
#HIST_STAMPS="dd/mm/yyyy" # Show timestamps is history output

# ------------------------------------ rbenv Enviromental include (Ruby)
rbenvactivate () {
    export PATH="$HOME/.rbenv/shims:$PATH"
    export RUBY_CONFIGURE_OPTS="--with-openssl-dir=$(brew --prefix openssl@1.1)"
    if ! type rbenv > /dev/null; then
        eval "$(rbenv init -)"
    fi
}

# ------------------------------------ NVM Environmental include (Node)
nvmactivate () {
    export NVM_DIR="$HOME/.nvm"
    [ -s "/usr/local/opt/nvm/nvm.sh" ] && . "/usr/local/opt/nvm/nvm.sh"  # This loads nvm
    [ -s "/usr/local/opt/nvm/etc/bash_completion.d/nvm" ] && . "/usr/local/opt/nvm/etc/bash_completion.d/nvm"  # This loads nvm bash_completion
}

# ------------------------------------ VirtualEnv Environmental include (Python)
# pip should only run if there is a virtualenv currently activated
export PIP_REQUIRE_VIRTUALENV=true

# Temporarily disable pip virtualenv requirement and install pip module.
gpip () {
    PIP_REQUIRE_VIRTUALENV="" pip "$@"
}

pyenvactivate () {
    # Use pyenv global
    if command -v pyenv 1>/dev/null 2>&1; then
        eval "$(pyenv init -)"
    fi
}

#pyenvactivate
#export PATH=$PATH:/Users/catilre/Library/Python/3.6/bin

# ------------------------------------ GoEnv Environmental include (GoLang)
goenvactivate () {
    eval "$(goenv init -)"
}
# ------------------------------------ GoLang Path Setup (GoLang)
export GOPATH=$(go env GOPATH)
export PATH=$PATH:$(go env GOPATH)/bin

espidfactivate () {
    cd ~/sites/espressif/esp-idf/
    pyenvactivate
    . ./export.sh
    cd -
}

# ------------------------------------ Azure-CLI bash completion
if [ ! -f /usr/local/Cellar/azure-cli/current/etc/bash_completion.d/az ]; then
    cd /usr/local/Cellar/azure-cli && unlink current || true && ln -s `ls` current && cd ~
fi
source /usr/local/Cellar/azure-cli/current/etc/bash_completion.d/az

# ------------------------------------ Mealie Crypt bash completion
eval "$(mealie-crypt --completion-script-zsh)"

# ------------------------------------ Directory info.
local current_dir='${PWD/#$HOME/~}'

export E=/Volumes/External\ Flash
export EHOME=$E/$HOME

# ------------------------------------ Git info.
local git_info='$(git_prompt_info)$(git_prompt_status)'
ZSH_THEME_GIT_PROMPT_ADDED="%{$fg[green]%}+"
ZSH_THEME_GIT_PROMPT_MODIFIED="%{$fg[magenta]%}!"
ZSH_THEME_GIT_PROMPT_DELETED="%{$fg[red]%}-"
ZSH_THEME_GIT_PROMPT_RENAMED="%{$fg[blue]%}>"
ZSH_THEME_GIT_PROMPT_UNMERGED="%{$fg[cyan]%}#"
ZSH_THEME_GIT_PROMPT_UNTRACKED="%{$fg[yellow]%}?"

ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg[cyan]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX=""
ZSH_THEME_GIT_PROMPT_DIRTY=""
ZSH_THEME_GIT_PROMPT_CLEAN=" %{$fg[green]%}✔"

# ------------------------------------ Vagrant info
# Get the current state of the vagrant vm in this folder
local vagrant_info='$(vagrant_info)'

# ------------------------------------ Prompts
# Left Prompt

local ret_status="%(?:%{$fg_bold[green]%}➜ :%{$fg_bold[red]%}➜ )"
PROMPT="%{$terminfo[bold]$fg[blue]%}# \
%{$fg[cyan]%}%n\
%{$fg[white]%}@\
%{$fg[green]%}$(box_name)\
%{$fg[white]%}:\
%{$terminfo[bold]$fg[yellow]%}${current_dir}%{$reset_color%} \
%{$fg[blue]%}${vagrant_info}%{$reset_color%}\
${git_info}%{$reset_color%}
%{$terminfo[bold]$fg[red]%}$ ${ret_status} %{$reset_color%}"

# Right Prompt
RPROMPT='[%*]'

# ------------------------------------ Caret colour
if [[ $USER == "root" ]]; then
    CARETCOLOR="red"
else
    CARETCOLOR="white"
fi

# ------------------------------------ Jetbrains
jbeval () {
#    # Remove eval key
#    find ~/Library/Preferences | grep "rubymine|storm|pycharm" | grep evaluation.key | xargs rm
#    find ~/Library/Preferences | grep "rubymine|storm|pycharm" | grep "eval" | xargs rm
#
#    # remove evl properties
#    for file in `ls ~/Library/Preferences/**/options/other.xml`; do
#        grep 'evl' $file && sed '/evl/d' $file > ${file}
#    done
#
#    # remove jb plists
#    rm ~/Library/Preferences/*jetbrains*.plist

    # Reset Intellij evaluation
    for product in IntelliJIdea WebStorm DataGrip PhpStorm CLion PyCharm GoLand RubyMine Rider; do
        echo "Resetting trial period for $product"

        echo "removing evaluation key..."
        rm -rf ~/Library/Preferences/$product*/eval

        # Above path not working on latest version. Fixed below
        rm -rf ~/Library/Application\ Support/JetBrains/$product*/eval

        echo "removing all evlsprt properties in options.xml..."
        sed -i '' '/evlsprt/d' ~/Library/Preferences/$product*/options/other.xml

        # Above path not working on latest version. Fixed below
        sed -i '' '/evlsprt/d' ~/Library/Application\ Support/JetBrains/$product*/options/other.xml

        echo
    done

    echo "removing additional plist files..."
    rm -f ~/Library/Preferences/com.apple.java.util.prefs.plist
    rm -f ~/Library/Preferences/com.jetbrains.*.plist
    rm -f ~/Library/Preferences/jetbrains.*.*.plist

    echo
    echo "That's it, enjoy ;)"
}

# ------------------------------------ Aliases
if type nvim > /dev/null 2>&1; then
  alias vim='nvim'
fi

# Misc
alias l='ls -lhF'
alias ll='ls -lahF'
alias cp='cp -iv'
alias mv='mv -iv'
alias qfind="find . -name "
alias rsync="rsync -avzP"
alias gpr='hub pull-request'
alias gcl='git_owner_clone'
alias sgit='sudo SSH_AUTH_SOCK="$SSH_AUTH_SOCK" git'
alias ddd="dcfldd statusinterval=10 status=on bs=1024"
alias rpg="postgres -D /usr/local/var/postgres > /dev/null 2>&1"
alias bex="bundle exec"
alias subl="/Applications/Sublime\ Text.app/Contents/SharedSupport/bin/subl"
alias tpl='tail -f -n50 /var/log/php_error_log'

# SSH Keys
alias ssh-key-home='ssh-add -D; ssh-add ~/.ssh/catilre ~/.ssh/pi ~/.ssh/catilre_pfsense ~/.ssh/catilre_jump'
alias ssh-key-bhm='ssh-add -D; ssh-add ~/.ssh/catilre ~/.ssh/alexp_bh ~/.ssh/devops_bh ~/.ssh/root_bh'

# Docker
alias dsalt='docker exec -ti salt-master'
alias dlogs='docker logs --tail 100 -f'
alias dslogs='docker service logs --tail 100 -f'
alias dsup='docker service update --force'

# SilverSearcher
alias ack='ag'
alias grep='ack'

# Mealie-Crypt
alias mc='mealie-crypt'
alias mce='mc encrypt' # -u alex.pullen -k ~/.ssh/alexp_bh'
alias mcd='mc decrypt' # -u alex.pullen -k ~/.ssh/alexp_bh'
alias mcvg='mcg'

# Phone
alias fixiphone='sudo killall usbd; sudo killall -STOP -c usbd'
alias fixphone='fixiphone'
alias chargeiphone='fixiphone'
alias chargephone='fixiphone'

# Mirror
alias mirroroff='ssh mirror.lan vcgencmd display_power 0'
alias mirroron='ssh mirror.lan vcgencmd display_power 1'
alias moff=mirroroff
alias mon=mirroron

# BHM
alias aves='ansible-vault encrypt_string'
alias today='vim -O /Volumes/britehouse-info/today.yaml' #-O /Volumes/britehouse-info/BHM-INFO/mealie-crypt/mealie-crypt.yaml'
alias bhminfo='open ~/Library/Mobile\ Documents/com\~apple\~CloudDocs/Documents/britehouse-info.dmg'
alias bhm='cd /Volumes/britehouse-info'
alias b101='ssh prd-1-0-1.atajo.co.za sudo /opt/bounce-core.sh'
alias t101='ssh prd-1-0-1.atajo.co.za sudo tail -f /apps/Atajo-Core-2.0/logs/main.log'
alias tb101='ssh prd-1-0-1.atajo.co.za sudo tail -f /var/log/core-bounce.log'
alias te101='ssh prd-1-0-1.atajo.co.za sudo tail -f /apps/Atajo-Core-2.0/logs/error.log'
alias b202='ssh prd-2-0-2.atajo.co.za sudo /opt/bounce-core.sh'

# Powershell
#alias pwsh='/usr/local/microsoft/powershell/6/pwsh'
#alias powershell='pwsh'

# Speed up time maching backup by not throttling for lower cpu usage.
alias tmturbo='sudo sysctl debug.lowpri_throttle_enabled=0'
alias tmsnail='sudo sysctl debug.lowpri_throttle_enabled=1'
alias timemachineturbo='tmturbo'
alias timemachinesnail='tmsnail'

# ESP-IDF
alias get_idf='espidfactivate'
# tmp store find store

# Path stuff
#export PATH="$PATH:~/.composer/vendor/bin:~/.composer/vendor/homestead:~/pear/bin/"
