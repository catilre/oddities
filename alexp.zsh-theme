#dev Directory info.
local current_dir='${PWD/#$HOME/~}'

# Developers initials
dev_inits='asp'

# Git info.
local git_info='$(git_prompt_info)$(git_prompt_status)'
ZSH_THEME_GIT_PROMPT_ADDED="%{$fg[green]%}+"
ZSH_THEME_GIT_PROMPT_MODIFIED="%{$fg[magenta]%}!"
ZSH_THEME_GIT_PROMPT_DELETED="%{$fg[red]%}-"
ZSH_THEME_GIT_PROMPT_RENAMED="%{$fg[blue]%}>"
ZSH_THEME_GIT_PROMPT_UNMERGED="%{$fg[cyan]%}#"
ZSH_THEME_GIT_PROMPT_UNTRACKED="%{$fg[yellow]%}?"

ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg[cyan]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX=""
ZSH_THEME_GIT_PROMPT_DIRTY=""
ZSH_THEME_GIT_PROMPT_CLEAN=" %{$fg[green]%}✔"

# Vagrant state.
vagrant_state () {
    if [ -f ./Vagrantfile ] && [ -f .vagrant/machines/default/virtualbox/id ]; then
        runningVMS=`vboxmanage list runningvms`
        if [ "$runningVMS" != "" ]; then
            runningVMS=`vagrant status | grep '(virtualbox)' | awk '{print $2}'`
            if [ "$runningVMS" = "running" ]; then
                echo "<= ON"
            else
                echo "<= $runningVMS"
            fi
        else
            echo '<= all off'
        fi
	elif [ -f ./Vagrantfile ]; then
		echo "=> it's dead Jim"
    else
    	echo ''
    fi
}

#update DNS
updateDNS () {
    cli53 rrcreate --replace dev.seecrypt.io $1'-'$dev_inits' 30 A '$2
}

# Vagrant IPs
vagrant_ips () {
	if [[ "$(vagrant_state)" = '<= ON' ]]; then
		BOX_NAME=`grep 'config\.vm\.hostname' -i VagrantFile | cut -f2 -d'=' | sed 's/[\"\ ]//g'`

        BOX_IP=`ssh $BOX_NAME 'ip a' | grep eth2 | grep inet | awk '{ print $2 }' | cut -f1 -d'/'`
		if [[ -n $BOX_IP ]]; then
            if [[ $1 == 'ip' ]]; then
			    `ssh $BOX_NAME 'sudo ifdown eth2 && sudo ifup eth2'`
                BOX_IP=`ssh $BOX_NAME 'ip a' | grep eth2 | grep inet | awk '{ print $2 }' | cut -f1 -d'/'`
		    fi

            if [[ $2 != "" ]]; then
                if [[ $2 == 'b' ]]; then 
                    updateDNS 'backends' $BOX_IP
                elif [[ $2 == 'd' ]]; then
                    updateDNS 'dev' $BOX_IP
                elif [[ $2 == 't' ]]; then
                    updateDNS 'testbox' $BOX_IP
                else
                    echo "Usage $0 [b|f]"
                fi
            fi
            
            if [[ "$(check_dns)" != "$BOX_IP" ]]; then
                echo "<= $BOX_IP%{$fg[red]%}!!%{$fg[blue]%}"
            else
                echo "<= $BOX_IP"
            fi
		else
            if [[ $1 == 'ip' ]]; then
				echo '$BOX_NAME has no public IP: '
			fi
			echo $(vagrant_state)
		fi
	else
		echo $(vagrant_state)
	fi
}

local vagrant_info='$(vagrant_ips)'

# DNS Matches
function check_dns {
	echo `nslookup dev-${dev_inits}.dev.seecrypt.io | grep -e 'Address' | grep -ve '#53' | cut -f2 -d' '`
}

# Boxname
function box_name {
    [ -f ~/.box-name ] && cat ~/.box-name || echo $SHORT_HOST || echo $HOST
}

# Prompts
PROMPT="%{$terminfo[bold]$fg[blue]%}# \
%{$fg[cyan]%}%n\
%{$fg[white]%}@\
%{$fg[green]%}$(box_name)\
%{$fg[white]%}:\
%{$terminfo[bold]$fg[yellow]%}${current_dir}%{$reset_color%} \
%{$fg[blue]%}${vagrant_info}%{$reset_color%}\
${git_info}%{$reset_color%}
%{$terminfo[bold]$fg[red]%}$ %{$reset_color%}"

RPROMPT='[%*]'


if [[ $USER == "root" ]]; then
  CARETCOLOR="red"
else
  CARETCOLOR="white"
fi

# Aliases
alias l='ls -lh'
alias ll='ls -lah'
alias cp='cp -iv'
alias mv='mv -iv'
alias qfind="find . -name "
alias rsync="rsync -avzP"
alias dd="dcfldd --statusinterval=100 --status=on"

# -> restart services
alias rgjs="sudo systemctl restart gearman-job-server"
alias rsc="sudo systemctl restart secure-backend"
alias recs="sudo supervisorctl restart all"
alias rrs="sudo systemctl restart redis-server"

alias rall="rgjs; rsc; recs; rrs;"

#-> tail logs
alias tsc="sudo tail -f /var/log/secure-application-server/*"
alias tapi="sudo tail -f /var/log/secure_api/api.log"
alias tecs="sudo tail -f /var/log/secure_ecs/ecs.log"
alias tngx="sudo tail -f /var/log/nginx/*.log"

alias tall="tsc; tapi; tecs; tngx;"
