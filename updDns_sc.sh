#!/bin/bash

updateDHCP () {
	runningVMS=`vboxmanage list runningvms`
	if [ "$runningVMS" != "" ]; then
		BOX_NAME=`pwd`
		BOX_NAME=`echo ${BOX_NAME##*/} | cut -f1 -d'.'`
		echo $runningVMS | grep -iq `cat .vagrant/machines/*/virtualbox/id`
		if [[ $? < 1 ]]; then
			`ssh $BOX_NAME.x 'sudo ifdown eth2 && sudo ifup eth2'`
		fi
	fi
}

update () {
	WHOLU='asp'
	runningVMS=`vboxmanage list runningvms`
	if [ "$runningVMS" != "" ]; then
		BOX_NAME=`pwd`
		BOX_NAME=`echo ${BOX_NAME##*/} | cut -f1 -d'.'`
		echo $runningVMS | grep -iq `cat .vagrant/machines/*/virtualbox/id`
		if [[ $? < 1 ]]; then
			BOX_IP=`ssh $BOX_NAME.x 'ip a' | grep eth2 | grep inet | awk '{ print $2 }' | cut -f1 -d'/'`
			if [[ -n $BOX_IP ]]; then
				#echo $1'-asp 30 A '$BOX_IP
				cli53 rrcreate --replace dev.seecrypt.io $1'-'$WHOLU' 30 A '$BOX_IP
			else
				echo '$BOX_NAME has no public IP'
			fi
		else
			echo '$BOX_NAME is off'
		fi
	else
		echo 'There are no VMs running'
	fi
}

if [[ $2 == 'ip' ]]; then 
	updateDHCP
fi

if [[ $1 == 'b' ]]; then 
	update 'backends'
elif [[ $1 == 'd' ]]; then
	update 'dev'
elif [[ $1 == 't' ]]; then
	update 'testbox'
else
	echo "Usage $0 [b|f]"
fi
