" Pathogen Plugin manager Autoloader ------------------------------------------
" execute pathogen#infect()

" Basic operations -------------------------------------------------------------
set nocompatible          " Use vim instead of vi settings; this must come first
colorscheme slate " default slate  Set colorscheme before anything else so it can be overidden
filetype off

" Vundle plugins ---------------------------------------------------------------
" run `vim +PluginInstall +qall` on command line to update installed pluginsfrom the below list.
" from within vim ':PluginInstall'
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
    Plugin 'VundleVim/Vundle.vim'
    Plugin 'vim-airline/vim-airline'
    Plugin 'scrooloose/nerdtree'
    Plugin 'scrooloose/nerdcommenter'
    Plugin 'Xuyuanp/nerdtree-git-plugin'
    Plugin 'nathanaelkane/vim-indent-guides'
    Plugin 'tpope/vim-fugitive'
    Plugin 'mhinz/vim-signify'
    Plugin 'sirtaj/vim-openscad'
    Plugin 'dart-lang/dart-vim-plugin'
    Plugin 'saltstack/salt-vim'
    Plugin 'mdempsky/gocode', {'rtp': 'vim/'}
    Plugin 'Glench/Vim-Jinja2-Syntax'
    Plugin 'majutsushi/tagbar' " - conflicts with macports during installation.
    Plugin 'ctrlpvim/ctrlp.vim'
    Plugin 'w0rp/ale' "Auto linting
    " Plugin 'vim-syntastic/syntastic' https://github.com/vim-airline/vim-airline/blob/master/doc/airline.txt#L493

    Plugin 'chr4/nginx.vim' "nginx syntax highlighting
    Plugin 'fatih/vim-go'
    " Plugin 'leafgarland/typescript-vim'
    " Plugin 'kchmck/vim-coffee-script'

    Plugin 'kaicataldo/material.vim'
call vundle#end()

" Basic operations 2 -----------------------------------------------------------
syntax on                 " Enable syntax highlighting
set encoding=utf-8        " Set encoding to utf-8
set fileformats=unix,dos
filetype plugin indent on
set path+=**              " search subfolders for tab completion

" CtrlP stuff ------------------------------------------------------------------
let g:ackprg = 'ag --nogroup --nocolor --column'

" PHP Stuff --------------------------------------------------------------------
" - Check syntax with Ctrl + L
autocmd FileType php noremap <C-L> :!/usr/bin/env php -l %<CR>
autocmd FileType phtml noremap <C-L> :!/usr/bin/env php -l %<CR>

" Ruby Stuff -------------------------------------------------------------------
" - set tabwith to 2 spaces with ruby files.
autocmd FileType haml setlocal shiftwidth=2 tabstop=2
autocmd FileType ruby setlocal shiftwidth=2 tabstop=2

" GoLang Stuff
"autocmd FileType go noremap <leader>r <Plug>(go-run)
"autocmd FileType go noremap <leader>b <Plug>(go-build)
"autocmd FileType go noremap <leader>t <Plug>(go-test)
"autocmd FileType go noremap <leader>c <Plug>(go-coverage)
" - Enable goimports to automatically insert import paths intead of gofmt
"let g:go_fmt_command = "goimports"

" NERDTree Stuff ---------------------------------------------------------------
" - Open NERDTree automatically when vim starts up when a folder is specified or when no files were specified
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | elseif argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | endif
" - Close vim if the only window left open is a NERDTree?
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

map <leader>n :NERDTreeToggle<CR>
let NERDTreeShowHidden=1
let NERDTreeShowLineNumbers=1
let NERDTreeWinSize=40
" NERDTree-Git Stuff -----------------------------------------------------------
let g:NERDTreeGitStatusIndicatorMapCustom = {
    \ "Modified"  : "!",
    \ "Staged"    : "+",
    \ "Untracked" : "?",
    \ "Renamed"   : ">",
    \ "Unmerged"  : "#",
    \ "Deleted"   : "-",
    \ "Dirty"     : "✗",
    \ "Clean"     : "✔︎",
    \ 'Ignored'   : '☒',
    \ "Unknown"   : "?"
    \ }

" Vim-Airline Stuff ------------------------------------------------------------
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#formatter = 'unique_tail_improved'
let g:airline#extensions#branch#enabled = 1                 " Git Branch via tpope/vim-fugitive
let g:airline#extensions#branch#empty_message = ''          " Text when no branch is detected
let g:airline#extensions#branch#displayed_head_limit = 10   " Truncate long branch display name
let g:airline#extensions#branch#format = 2                  " Squish branch name
let g:airline#extensions#tabline#buffer_idx_mode = 1        " 1 index buffers.
  nmap <leader>1 <Plug>AirlineSelectTab1
  nmap <leader>2 <Plug>AirlineSelectTab2
  nmap <leader>3 <Plug>AirlineSelectTab3
  nmap <leader>4 <Plug>AirlineSelectTab4
  nmap <leader>5 <Plug>AirlineSelectTab5
  nmap <leader>6 <Plug>AirlineSelectTab6
  nmap <leader>7 <Plug>AirlineSelectTab7
  nmap <leader>8 <Plug>AirlineSelectTab8
  nmap <leader>9 <Plug>AirlineSelectTab9
  nmap <leader>- <Plug>AirlineSelectPrevTab
  nmap <leader>= <Plug>AirlineSelectNextTab
  nmap <leader>§ :bp<bar>sp<bar>bn<bar>bd<CR>

" Ale Stuff --------------------------------------------------------------------
" Error and warning signs.
let g:ale_sign_error = '⤫'
let g:ale_sign_warning = '⚠'

let g:airline#extensions#ale#enabled = 1 " Enable ale integration with airline.

" TagBar Stuff -----------------------------------------------------------------
nmap <F9> :TagbarToggle<CR>
let g:tagbar_type_ansible = {
    \ 'ctagstype' : 'ansible',
    \ 'kinds' : [
        \ 't:tasks',
        \ 'v:vars'
    \ ],
    \ 'sort' : 0
\ }

" Vim-Indent-Guides Stuff ------------------------------------------------------
let g:indent_guides_exclude_filetypes = ['help', 'nerdtree']
"let g:indent_guides_guide_size = 2
"let g:indent_guides_start_level = 2
let g:indent_guides_enable_on_vim_startup = 1
noremap <leader>ig :IndentGuidesToggle <CR>
"let g:indent_guides_auto_colors = 0
"autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  ctermbg=black
"autocmd VimEnter,Colorscheme * :hi IndentGuidesEven ctermbg=darkgrey

" Yaml Stuff -------------------------------------------------------------------
" - set tabwith to 2 spaces with yaml files.
" autocmd FileType yaml let &l:ts=2 sts=2 sw=2 expandtab
autocmd FileType yaml setlocal shiftwidth=2 tabstop=2 softtabstop=2 expandtab
" - perform yamllint with Ctrl + L or <leader> + y + l
autocmd FileType yaml noremap <C-L> :!clear; /usr/bin/env yamllint %<CR>
autocmd FileType yaml noremap <leader>yl :!clear; /usr/bin/env yamllint %<CR>

"Yaml lint
let g:yaml_limit_spell=1
autocmd BufNewFile,BufRead *.yaml,*.yml so ~/.vim/syntax/yaml.vim
autocmd BufWritePost *.yaml,*.yml !yamllint <afile>

" Ansible Stuff ----------------------------------------------------------------
" - perform ansible-lint with <leader> + a + l
autocmd FileType yaml noremap <leader>al :!clear; /usr/local/bin/ansible-lint %<CR>

" Salt Stuff
" Force using the Django template syntax file
let g:sls_use_jinja_syntax = 0
"set rdt=0

" Behaviors --------------------------------------------------------------------
" autocmd BufWritePost .vimrc so ~/.vimrc " automatically reload vimrc when it's saved
nnoremap <Space> a_<Esc>r
nnoremap H ^
nnoremap L $
nnoremap J G
nnoremap K 1G

set showcmd               " In Visual mode, show selected characters, lines, etc
set wildmenu              " Turn on menu-based tab completion for commands
set autoread              " Read file if it has changed outside of Vim
set splitbelow splitright " More intuitive than default split behavior
set nobackup              " Modern systems have better ways to back up files
set noswapfile            " Not much need for swapfiles in the 21st century
set nojoinspaces          " Use only one space after period when joining lines
set paste
set comments=sr:/*,mb:*,ex:*/ " docblock comments are continued when a newline is inserted
set scrolloff=7           " Never scroll to the edge of the window
set noerrorbells          " No bells!
set novisualbell          " I said, no bells!
set clipboard=unnamed     " Yank (Y) to OS clipboard when in iTerm2 in iTerm prefs check 'General->Applications in terminal may access clipboard'

" Tabs and spaces --------------------------------------------------------------
set shiftwidth=4          " Spaces to use for each indent step (>>, <<, etc.)
set shiftround            " Round indent to multiple of shiftwidth
set softtabstop=4         " Spaces to use for <tab> and <BS> editing operations
set expandtab             " Use appropriate # of spaces for <tab> in insert mode
set tabstop=4             " Tab key produces 4 spaces, and tab characters are converted to spaces

" Indents and wrapping ---------------------------------------------------------
set linebreak             " Soft-wrap at word instead of character
set autoindent            " Copy indent from current line when starting new line
set bs=indent,eol,start   " Backspace over autoindent, EOL, and start of insert

" Searching --------------------------------------------------------------------
set ignorecase            " Case-insensitive search
set smartcase             " Case-sensitive search if query contains uppercase
set incsearch             " Show first search result as query is typed
map <leader>/ :set hlsearch!<CR>

" Replacing --------------------------------------------------------------------
set inccommand=split
" Folding ----------------------------------------------------------------------
set foldmethod=indent
set foldcolumn=3
set foldnestmax=1
set foldlevel=0
set foldlevelstart=0
"set foldclose=all         " Folds close automatically when you move out of it
":setlocal diff foldmethod=diff scrollbind nowrap foldlevel=1 " Folding unchanged lines
highlight Folded guibg=darkgrey guifg=blue ctermbg=8 ctermfg=12
highlight FoldColumn guibg=darkgrey guifg=white ctermbg=8 ctermfg=12
" https://upload.wikimedia.org/wikipedia/commons/1/15/Xterm_256color_chart.svg

" Visual -----------------------------------------------------------------------
set cursorline            " Highlight current line
set ruler                 " Show a ruler
set number                " Show current line number instrad of 0 for below relative line numbers
set relativenumber        " Show lines numbers relative to where you are
"au InsertEnter * :set nu  " absolute line numbers in insert mode,
"au InsertLeave * :set rnu " relative otherwise for easy movement
set showmatch             " Show matching brackets
set listchars=tab:▸\ ,eol:¬,trail:⋅,extends:❯,precedes:❮ " Make the list chars less hideous (and more like textmate)
set showbreak=↪
"set background=dark
set noscrollbind
"set scrollopt diff
map <leader>sb :set scrollbind!<CR>

"set diffopt+=iwhite
set diffexpr=""

"JSON lint/format
map <leader>jf :%!python -m json.tool<CR>
autocmd BufWritePost *.json !jsonlint-cli <afile>

"XML Lint/format
map <leader>xf :silent %!xmllint --format --recover - 2>/dev/null<CR>

